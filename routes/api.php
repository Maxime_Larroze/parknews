<?php

use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\NewsletterController;
use App\Http\Controllers\API\SocialiteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('guest')->group(function () {
    Route::prefix('/v1/public')->group(function () {
        Route::get('/login', [SocialiteController::class, 'handleCallback'])->name('handle.callback');
        Route::get('/callback', [SocialiteController::class, 'redirectDriver'])->name('callback');

        Route::get('/newsletters/30', [NewsletterController::class, 'index'])->name('newsletters.index');
        Route::get('/newsletters', [NewsletterController::class, 'show'])->name('newsletters.index');
        Route::get('/categories', [CategoryController::class, 'index'])->name('api.categories.index');

    });
});



Route::middleware('auth')->group(function () {
    Route::prefix('/v1/auth')->group(function () {
        Route::get('/logout', [SocialiteController::class, 'logout'])->name('logout');
    });
});
