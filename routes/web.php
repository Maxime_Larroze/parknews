<?php

use App\Http\Controllers\API\SocialiteController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\EmailTemplateController;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\GithubController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\HistoryConnectionController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ViewController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('guest')->group(function () {
    Route::get('/', [ViewController::class, 'redirectLogin'])->name('login.redirect');
    Route::prefix('/public')->group(function () {
        Route::get('/', [ViewController::class, 'showLogin'])->name('login');

        Route::get('/google/callback', [GoogleController::class, 'handleGoogleCallback'])->name('callback.google');
        Route::get('/google', [GoogleController::class, 'redirectToGoogle'])->name('register.google');

        Route::get('/github/callback', [GithubController::class, 'handleGithubCallback'])->name('callback.github');
        Route::get('/github', [GithubController::class, 'redirectToGithub'])->name('register.github');

        Route::get('/facebook/callback', [FacebookController::class, 'handleFacebookCallback'])->name('callback.facebook');
        Route::get('/facebook', [FacebookController::class, 'redirectToFacebook'])->name('register.facebook');

        Route::get('/doc/api', [ViewController::class, 'showApi'])->name('api.show');
        Route::get('/doc/project', [ViewController::class, 'showProject'])->name('project.show');
    });

});

Route::middleware('auth')->group(function () {
    Route::prefix('/auth')->group(function () {
        Route::get('/logout', [UserController::class, 'logout'])->name('user.logout');
        Route::get('/home', [ViewController::class, 'showHome'])->name('home');

        Route::get('/clients', [ClientController::class, 'index'])->name('clients.index');
        Route::post('/clients', [ClientController::class, 'store'])->name('clients.store');
        Route::put('/clients', [ClientController::class, 'update'])->name('clients.update');
        Route::delete('/clients', [ClientController::class, 'destroy'])->name('clients.destroy');

        Route::get('/me', [UserController::class, 'show'])->name('me.show');
        Route::put('/me', [UserController::class, 'update'])->name('me.update');

        Route::get('/newsletters', [NewsletterController::class, 'show'])->name('newsletters.show');
        Route::post('/newsletters', [NewsletterController::class, 'store'])->name('newsletters.store');
        Route::put('/newsletters/publish', [NewsletterController::class, 'edit'])->name('newsletters.edit');
        Route::put('/newsletters', [NewsletterController::class, 'update'])->name('newsletters.update');
        Route::delete('/newsletters', [NewsletterController::class, 'destroy'])->name('newsletters.destroy');
        Route::get('/newsletters/{id}', [NewsletterController::class, 'showUpdate'])->name('newsletters.showUpdate');
        Route::post('/newsletters/search/result', [NewsletterController::class, 'search'])->name('newsletters.search');

        Route::get('/newsletters/process/send', [NewsletterController::class, 'showSender'])->name('newsletter.showsender');
        Route::post('/newsletters/process/send', [NewsletterController::class, 'send'])->name('newsletters.send');

        Route::post('/public/newsletters/register', [ClientController::class, 'create'])->name('clients.create');

        Route::get('/template-emails', [EmailTemplateController::class, 'show'])->name('mailings.show');
        Route::post('/template-emails', [EmailTemplateController::class, 'store'])->name('mailings.store');
        Route::put('/template-emails/publish', [EmailTemplateController::class, 'edit'])->name('mailings.edit');
        Route::put('/template-emails', [EmailTemplateController::class, 'update'])->name('mailings.update');
        Route::post('/template-emails/test', [EmailTemplateController::class, 'send'])->name('mailings.send');
        Route::delete('/template-emails', [EmailTemplateController::class, 'destroy'])->name('mailings.destroy');

        Route::middleware('admin')->group(function () {
            Route::prefix('/admin')->group(function () {
                Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
                Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
                Route::put('/categories/publish', [CategoryController::class, 'edit'])->name('categories.edit');
                Route::put('/categories', [CategoryController::class, 'update'])->name('categories.update');
                Route::delete('/categories', [CategoryController::class, 'destroy'])->name('categories.destroy');

                Route::get('/users', [UserController::class, 'index'])->name('users.index');
                Route::put('/users', [UserController::class, 'edit'])->name('users.edit');
                Route::delete('/users', [UserController::class, 'destroy'])->name('users.destroy');

                Route::delete('/logs', [HistoryConnectionController::class, 'store'])->name('history.store');
            });
        });

    });
});

Route::get('/public/newsletters', [ViewController::class, 'showBlog'])->name('newsletters.signed');
Route::get('/public/informations', [NewsController::class, 'show'])->name('news.show');
Route::post('/public/informations', [NewsController::class, 'search'])->name('news.search');

Route::middleware('signed')->group(function(){
    Route::get('/public/newsletters/read/{id}', [NewsletterController::class, 'index'])->name('public.newsletters.signed');
    Route::get('/public/unsubscribe/newsletter/user/{user}/mail/{mail}', [NewsletterController::class, 'unsubscribe'])->name('public.unsubscribe.signed');
});

Route::fallback(function() {
    return redirect()->route('home');
});
