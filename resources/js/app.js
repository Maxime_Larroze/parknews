require('./bootstrap');
window.Vue = require('vue').default;
window.Chart = require('chart.js').default;


window.Quill = require('quill');

window.Quill.register({
  'modules/toolbar': Toolbar,
  'themes/snow': Snow,
  'formats/bold': Bold,
  'formats/italic': Italic,
  'formats/header': Header
});
