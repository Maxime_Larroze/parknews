import * as Vue from 'vue';
import axios from 'axios';
import * as Chart from 'chart.js'
import { method, parseInt } from 'lodash';
const Axios = axios.create({
    baseURL: 'http://parknews.services-ehe.fr/api',
    version:'v1'
});

const donutChart = new window.Vue({
    el:'#donutChart',
    data:
    {
        LesCategories:[],
        LesData:[],
        title:"toto"
    },
    mounted()
    {
        this.getDataForDonut().then((res)=>{
            this.LesCategories=Object.keys(res);
            this.LesData=Object.values(res);
            this.getChart();
        }).catch((err)=>{
            console.log(err)
        })
    },
    computed:
    {
    },
    methods:
    {
        getChart()
        {
            new window.Chart("donutChartJS", {
            type: "doughnut",
            data: {
              labels: this.LesCategories,
              datasets:
              [{
                backgroundColor:  [
                    "#A93226",
                    "#CB4335",
                    "#884EA0",
                    "#7D3C98",
                    "#2471A3",
                    "#17A589",
                    "#138D75",
                    "#229954",
                    "#28B463",
                    "#D4AC0D",
                    "#D68910",
                    "#CA6F1E",
                    "#BA4A00",
                    "#D0D3D4",
                    "#839192",
                    "#707B7C",
                    "#2E4053",
                    "#273746",
                  ],
                data: this.LesData
              }]
            },
            options: {
              title: {
                display: true,
                text: this.title
              }
            }
          });
        },
        async getDataForDonut()
        {
            try
            {
              const res =  await Axios.get(`v1/public/categories`);
              //plutôt que de faire data=res.data je peux faire ->
              return res.data
            }catch(err)
            {
               console.log(err)
            }
        }
    }
})


const lineChart = new window.Vue({
    el:'#lineChart',
    data:
    {
         xValues : [1],
         yValues :[15],
    },
    mounted()
    {
        this.getDataForLine().then((res)=>{
            this.xValues = Object.keys(res);
            this.yValues =Object.values(res);
            this.getLineChart();
        }).catch((err)=>{
            console.log(err);
        });
    },
    computed:
    {
    },
    methods:
    {
        getLineChart()
        {
            return new window.Chart("lineChartJS", {
                type: "line",
                data: {
                  labels: this.xValues,
                  datasets: [{
                    fill: false,
                    pointRadius: 2,
                    borderColor: "rgba(0,0,255,0.5)",
                    data: this.yValues
                  }]
                },
                options: {
                  legend: {display: false},
                  title: {
                    display: false,
                    text: "y = x * 2 + 7",
                    fontSize: 16
                  }
                }
              });
        },
        async getDataForLine()
        {
            try
            {
              const res =  await Axios.get('v1/public/newsletters/30');
              //plutôt que de faire data=res.data je peux faire ->
              return res.data
            }catch(err)
            {
               console.log(err)
            }
        }
    }
})


const newsletters = new window.Vue({
    el: '#newsletter',
    data: {
        LesNewsletter:[],
        code:0,
    },

    created()
    {
        this.getNewsletter().then((res)=>{
            this.LesNewsletter=res.data;
            this.code=res.status
        }).catch((err)=>{
            console.log(err);
        })
    },
    methods:
    {
       async getNewsletter()
        {
            try
            {

              const res =  await Axios.get(`v1/public/newsletters`);
              //plutôt que de faire data=res.data je peux faire ->
               return res
            }catch(err)
            {
               console.log(err)
            }
        }
    }
});

