@include('template.header')
<body>
<div class="bg_img"></div>
@include('template.navbar-top')
@auth
    @include('template.navbar-left')
@endauth
<main class="my-5">
    <div class="container">
        <div class="row">
            @yield('public.blog.article.show')
            @yield('public.blog.list_articles.show')
        </div>
    </div>
  </main>
@include('template.footer')
