@extends('public.blog')
@section('public.blog.article.show')
<div class="card">
    <div class="card-body" style="opacity: 0.9"><h2 class="text-center">{{$newsletter->title}}</h2></div>
    <div class="row">
        <div class="col-8 mb-3 mt-3">
            <hr class="mt-3">
            <div class="row">
                <div class="col-6">
                    <p>Edité le {{\Carbon\Carbon::parse($newsletter->created_at)->format('d/m/Y H:i')}}</p>
                    <p>Dernière modification le {{\Carbon\Carbon::parse($newsletter->updated_at)->format('d/m/Y H:i')}}</p>
                </div>
                <div class="col-6">
                    @foreach($ent_users as $ent_user)
                        @if($newsletter->user_id === $ent_user->id)
                            <p>Edité par {{$ent_user->society_name}}</p>
                            <p><a href="mailto:{{$ent_user->society_email}}">{{$ent_user->society_email}}</a> - <a href="tel:{{$ent_user->society_phone}}">{{$ent_user->society_phone}}</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <hr class="mt-3">
        </div>
        <div class="col-4 mb-3 mt-3">
            @if($newsletter->link_type === "video")
                <iframe width="100%" height="100%" class="radius" src="{{$newsletter->link}}" title="{{$newsletter->title}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            @elseif($newsletter->link_type === "picture")
                <img src="{{$newsletter->link}}" class="img-fluid" alt="{{$newsletter->link}}">
            @elseif($newsletter->link_type === "link")
                <a href="{{$newsletter->link_type}}" alt="{{$newsletter->link_type}}">{{$newsletter->link_type}}</a>
            @else
                <a href="{{$newsletter->link}}" alt="{{$newsletter->link_type}}">{{$newsletter->link}}</a>
            @endif
        </div>
        <div class="col-12 mb-3 mt-3">
            <p>{!! str_replace("<script>", "", str_replace("</script>", "", $newsletter->description)) !!}</p>
        </div>
        @auth
            <div class="col-12 mb-3 mt-3">
                <hr class="mt-3">
                <h3 class="text-center">Vous aimez ?</h3>
            </div>
            <form action="{{route('clients.create')}}" method="post">
                <div class="row">
                        <div class="col-8">
                                @csrf
                                <input type="hidden" name="newsletter_id" value="{{$newsletter->id}}">
                                @foreach($ent_users as $ent_user)
                                    @if($newsletter->user_id === $ent_user->id)
                                        <input type="hidden" name="user_id" value="{{$ent_user->id}}">
                                    @endif
                                @endforeach
                                <div class="form-floating mb-3 mt-3">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Votre adresse email" value="{{Auth::user()->email}}">
                                    <label for="description">Votre adresse email</label>
                                    @error('description')
                                        <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                                    @enderror
                                </div>
                        </div>
                        <div class="col-4 form-floating mb-3 mt-4 text-center">
                            <button class="btn btn-primary">Enregistrer</button>
                        </div>
                        <div class="col-12">
                            @error('error')
                                <div class="alert alert-danger alert-outline alert-dismissible" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    <div class="alert-icon"><i class="far fa-bell"></i></div>
                                    <div class="alert-message">
                                        {{$message}}
                                    </div>
                                </div>
                                @enderror
                                @error('success')
                                <div class="alert alert-success alert-outline alert-dismissible" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    <div class="alert-icon"><i class="fas fa-check"></i></div>
                                    <div class="alert-message">
                                        {{$message}}
                                    </div>
                                </div>
                            @enderror
                        </div>
                </div>
            </form>
        @endauth
    </div>
</div>

@endsection
