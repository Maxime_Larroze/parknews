@extends('public.blog')
@section('public.blog.article.show')
<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="{{route('news.search')}}" method="post">
                @csrf
                <div class="input-group">
                        <input class="form-control" id="search" name="search" type="text" placeholder="Rechercher un article" aria-label="Rechercher" value="{{$search??old('search')}}" required>
                        <button class="btn btn-primary border-end border-white" type="submit"><i class="fas fa-search"></i></button>
                        @error('search')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                </div>
            </form>
        </div>
        @if(!empty($resultats))
        <div class="col-12 mt-3 text-center">
            <h4 class="mb-5 text-white"><strong>Résultats de votre recherche: {{$search}}</strong></h4>
            <div class="row">
                @if($resultats->isEmpty())
                    <div class="mt-4 p-5 rounded bg-white">
                        <h3>Aucun résultat ne correspond à votre recherche: {{$search}}</h3>
                    </div>
                @else
                    @foreach($resultats as $resultat)
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 col-xxl-3 mb-4">
                            <div class="card">
                                <img src="{{$resultat->link_picture}}" class="card-img-top" />
                                <div class="card-body">
                                    <h5 class="card-title">{{ substr($resultat->title, 0, 22) }} ...</h5>
                                    <p class="card-text">
                                        {{substr(str_replace("<script>", "", str_replace("</script>", "", $resultat->description)), 0, 20)}} ...
                                    </p>
                                    <a href="{{URL::signedRoute('public.newsletters.signed', ['id'=>$resultat->id])}}" class="btn btn-primary">Lire</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        @endif
        <div class="col-12 mt-5">
            <div class="row text-center">
                <h4 class="mb-5 text-white"><strong>Dernières news</strong></h4>
                    @foreach($news as $new)
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 col-xxl-3 mb-4">
                            <div class="card">
                                <img src="{{$new['urlToImage']}}" class="card-img-top" />
                                <div class="card-body">
                                    <h5 class="card-title">{{ substr($new['title'], 0, 22) }} ...</h5>
                                    <p class="card-text">
                                        {{substr(str_replace("<script>", "", str_replace("</script>", "", $new['description'])), 0, 20)}} ...
                                    </p>
                                    <a href="{{$new['url']}}" target="_blank" class="btn btn-primary">Lire</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
    </div>
</div>
@endsection
