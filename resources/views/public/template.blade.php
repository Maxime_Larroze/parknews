@include('template.header')
<body class="user-select-none">
<video playsinline autoplay muted loop id="bgvid">
    <source src="../videos/background-04.mp4" type="video/mp4">
        Votre navigateur ne supporte pas l'extension vidéo
</video>
<div class="container">
    <main class="d-flex w-100">
		<div class="container d-flex flex-column">
			<div class="row vh-100">
				<div class="col-12 col-sm-12 col-md-10 col-lg-6 col-xl-5 col-xxl-4 mx-auto d-table h-100">
					<div class="d-table-cell align-middle">
						<div class="card radius bg-tr-50">
							<div class="card-body">
								<div class="m-sm-4 row">
                                    <div class="col-12 col-lg-12">
                                        <div class="tab">
                                            <div class="text-center">
                                                <img src="{{asset('/images/logo/logo.png')}}" alt="Logo ParkNews" class="img-fluid rounded-circle" width="200" height="200" />
                                            </div>
                                            @error('error')
                                                <div class="alert alert-danger alert-outline alert-dismissible mt-3" role="alert">
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                                    <div class="alert-icon"><i class="fas fa-exclamation-triangle"></i></div>
                                                    <div class="alert-message text-center">{{$message}}</div>
                                                </div>
                                            @enderror
                                            @error('success')
                                                <div class="alert alert-success alert-outline alert-dismissible mt-3" role="alert">
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                                    <div class="alert-icon"><i class="fas fa-check"></i></div>
                                                    <div class="alert-message text-center">{{$message}}</div>
                                                </div>
                                            @enderror
                                            <div class="bg-transparent text-center mt-5">
                                                <a class="text-danger" href="{{route('register.google')}}">
                                                    <button type="submit" id="btn_submit" class="btn btn-danger text-white">
                                                        Se connecter avec Google <i class="fab fa-google"></i>
                                                    </button>
                                                </a>
                                                <div class="mt-3"></div>
                                                <a class="text-danger" href="{{route('register.facebook')}}">
                                                    <button type="submit" id="btn_submit" class="btn btn-primary text-white">
                                                        Se connecter avec Facebook <i class="fab fa-facebook"></i>
                                                    </button>
                                                </a>
                                                <div class="mt-3"></div>
                                                <a class="text-danger" href="{{route('register.github')}}">
                                                    <button type="submit" id="btn_submit" class="btn btn-dark text-white">
                                                        Se connecter avec Github <i class="fab fa-github"></i>
                                                    </button>
                                                </a>
                                                <div class="mt-3"></div>
                                                <a class="text-white" href="{{URL::signedRoute('newsletters.signed', 1)}}">
                                                    Le blog <i class="fas fa-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
@include('template.footer')
