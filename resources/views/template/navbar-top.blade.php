<nav class="sb-topnav navbar navbar-expand navbar-light bg-primary">
    @auth
        <button class="btn" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><i class="fas fa-bars fa-2x text-white"></i></button>
        <a href="{{URL::signedRoute('newsletters.signed', 1)}}" class="text-white"><i class="fas fa-cube fa-2x"></i></a>
    @endauth
    {{-- <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
        <div class="input-group">
            <input class="form-control" type="text" placeholder="Rechercher" aria-label="Rechercher" aria-describedby="btnNavbarSearch">
            <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search-plus"></i></button>
        </div>
    </form> --}}
    <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
        <form action="{{route('newsletters.search')}}" method="post">
            @csrf
            <div class="input-group">
                <input class="form-control" type="text" placeholder="Rechercher" aria-label="Rechercher" aria-describedby="btnNavbarSearch" name="search" value="{{old('search')}}" required>
                <button class="btn btn-primary" id="btnNavbarSearch" type="submit"><i class="fas fa-search-plus"></i></button>
                @error('search')
                    <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                @enderror
            </div>
        </form>
    </div>
    @auth
    <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
        <li class="nav-item"><a class="nav-link text-white" href="{{route('me.show')}}"><i class="fas fa-user fa-2x"></i></a></li>
        <li class="nav-item"><a class="nav-link text-danger" href="{{route('user.logout')}}"><i class="fas fa-power-off fa-2x"></i></a></li>
    </ul>
    @endauth
    @guest
        <a class="nav-link text-white" href="{{route('login')}}"><i class="fas fa-sign-in-alt fa-2x"></i></a>
    @endguest
</nav>
