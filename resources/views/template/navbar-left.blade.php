<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
    <div class="offcanvas-header">
      <br>
      <h5 class="offcanvas-title">Park News</h5>
      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <hr>
    <div class="offcanvas-body row">
        <div class="col-lg-12">
            <div class="position-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{route('home')}}"><i class="fas fa-columns"></i> Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('newsletters.show')}}"><i class="far fa-file-alt"></i> Newsletter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('news.show')}}"><i class="fas fa-rss"></i> Informations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('mailings.show')}}"><i class="far fa-newspaper"></i> Modèle de newsletter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('newsletter.showsender')}}"><i class="fas fa-paper-plane"></i> Envoyer une newsletter</a>
                    </li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                  <span>Administration</span>
                </h6>
                <ul class="nav flex-column mb-2">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('clients.index')}}"><i class="fas fa-users fa-1x"></i> Gestion des clients</a>
                    </li>
                    @if(Auth::user()->email === 'maxime.larroze.francezat@gmail.com')
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('categories.index')}}"><i class="fas fa-users fa-1x"></i> Gestion des catégories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('users.index')}}"><i class="fas fa-users fa-1x"></i> Gestion des utilisateurs</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
