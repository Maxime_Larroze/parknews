<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="Lyes Souifi, Maxime Larroze">
	<meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta http-equiv="X-Content-Type-Options" content="nosniff">

	<title>ParkNews</title>

	<link rel="shortcut icon" href="{{asset('images/logo/logo.png')}}" />
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/quill.css')}}">
    <script src="{{mix('js/app.js')}}"></script>
    <script src="{{mix('js/bundle.js')}}"></script>
    <script src="{{ asset('js/chart.js') }}"></script>

</head>

