<footer class="text-light bg-transparent fixed-bottom">
    <div class="container">
        <div class="row text-muted">
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-4 col-xxl-5 text-start">
                <p class="mb-0 text-muted">
                    <strong>ParkNews</strong> &copy;
                </p>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-8 col-xl-8 col-xxl-7 text-end">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a class="text-muted" href="{{route('project.show')}}" target="_blank">Documentation</a>
                    </li>
                    <li class="list-inline-item">-</li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="/docs/erd.html" target="_blank">Doc BDD</a>
                    </li>
                    <li class="list-inline-item">-</li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="{{route('api.show')}}" target="_blank">ParkNews API</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</body>
<script src="{{mix('js/vue.js')}}"></script>

</html>
