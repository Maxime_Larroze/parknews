@extends('auth.template')
@section('auth.home.menu.show')
<div class="container-fluid mt-5">
    <div class="row mb-5">
        @include('auth.home.widget.derniere_nl')
        @include('auth.home.widget.graph_categories')
        {{-- @include('auth.home.widget.graph_30j') --}}
    </div>
</div>
@endsection

