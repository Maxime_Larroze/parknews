<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3">
    <div class="card shadow mb-4" >
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Mes 8 dernières newsletters</h6>
        </div>
        <div class="card-body">
            @if ($newsletters->isEmpty())
                <h3 class="text-center">Vous n'avez encore édité aucune newsletter</h3>
            @else
            @foreach ($newsletters as $newsletter)
                <p><a href="{{URL::signedRoute('public.newsletters.signed', ['id'=>$newsletter->id])}}">{{$newsletter->title}}</a></p>
            @endforeach
            @endif
        </div>
    </div>
</div>
