<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3">
    <div class="card mb-4" id="donutChart">
        <div class="card-header">
            Nombres de newsletters par catégories
        </div>
        <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"></div></div><canvas id="donutChartJS" style="margin: 0 auto;" class="chartjs-render-monitor"></canvas></div>
    </div>
</div>
