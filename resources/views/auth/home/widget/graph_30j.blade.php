<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
    <div class="card mb-4" id="lineChart">
        <div class="card-header">
            Nombre de newsletters créées sur 30 jours
        </div>
        <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"></div></div><canvas id="lineChartJS" style="display: block; height: 350px; width: 836px;" class="chartjs-render-monitor"></canvas></div>
    </div>
</div>
