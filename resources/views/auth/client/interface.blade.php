@extends('auth.template')
@section('auth.client.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-12 mb-5">
            <p><a href="" data-bs-toggle="collapse" data-bs-target="#info">Informations <i class="far fa-question-circle text-primary"></i></a></p>
            <div id="info" class="collapse">
                <p>
                    Cette page vous permet de voir votre liste de clients (liste d'emails).
                    Vous pouvez à tout moment archiver ou supprimer un client. <br>
                    <br>
                    Si vous archivez un client, celui-ci ne perçoit pas de mail de publication.
                    L'archivage permet au client de faire une "pause" dans vos campagnes de mailing.
                </p>
            </div>
        </div>
        <div class="col-8 col-sm-8 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
            {{-- <div class="input-group">
                <input class="form-control" type="text" placeholder="Rechercher" aria-label="Rechercher" aria-describedby="btnNavbarSearch">
                <button class="btn btn-primary border-end border-white" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
            </div> --}}
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xxl-2 text-center">
            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#NewClientModal">Nouveau client</button>
        </div>
        <div class="col-12 mt-5 table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Etat</th>
                        <th>Enregistrement</th>
                        <th>Adresse email</th>
                        <th>Enregistré le</th>
                        <th>Options</th>
                    </tr>
                </thead>
                @if(!empty($clients))
                    <tbody>
                        @foreach($clients as $client)
                            <tr>
                                <th>@if(!empty($client->archived_at)) <span class="text-warning">Archivé</span> @else <span class="text-success">Actif</span> @endif</th>
                                <th>@if($client->register_type === 'interne') <span class="text-warning">{{$client->register_type}}</span> @else <span class="text-success">{{$client->register_type}}</span> @endif</th>
                                <td><a href="mailto:{{$client->email}}">{{$client->email}}</a></td>
                                <td>{{\Carbon\Carbon::parse($client->created_at)->format('d/m/Y')}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-6">
                                            <form action="{{route('clients.update')}}" method="post">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="client_id" value="{{$client->id}}">
                                                @if(empty($client->archived_at))
                                                        <button type="submit" class="btn btn-secondary rounded"><i class="fas fa-archive"></i></button>
                                                @else
                                                    <button type="submit" class="btn btn-warning text-white rounded"><i class="fas fa-sync"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                        <div class="col-6">
                                            <form action="{{route('clients.destroy')}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="client_id" value="{{$client->id}}">
                                                <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-trash-alt"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                @else
                    </table>
                    <h3 class="bold text-center">Aucun client enregistré pour l'instant</h3>
                @endif
        </div>
    </div>
</div>
@include('modals.new_client')
@endsection
