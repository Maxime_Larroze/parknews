@extends('auth.template')
@section('auth.profil.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-6 form-floating mb-3 mt-3">
            <input type="text" class="form-control" id="prenom" value="{{$user->firstname}}" disabled>
            <label for="prenom">Prénom</label>
        </div>
        <div class="col-6 form-floating mb-3 mt-3">
            <input type="text" class="form-control" id="nom" value="{{$user->lastname}}" disabled>
            <label for="nom">Nom</label>
        </div>
        <div class="col-12 form-floating mb-3 mt-3">
            <input type="text" class="form-control" id="user_email" value="{{$user->email}}" disabled>
            <label for="user_email">Adresse email</label>
            <hr>
        </div>
        <div class="col-12 form-floating mb-3 mt-3">
            <form action="{{route('me.update')}}" method="post">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-6 form-floating mb-3 mt-3">
                        <input type="text" class="form-control" name="society_name" id="society_name" value="{{$user->society_name ?? old('society_name')}}" required>
                        <label for="society_name">Nom de votre société</label>
                    </div>
                    <div class="col-6 form-floating mb-3 mt-3">
                        <input type="email" class="form-control" name="society_email" id="society_email" value="{{$user->society_email ?? old('society_email')}}" required>
                        <label for="society_email">Adresse email</label>
                    </div>
                    <div class="col-6 form-floating mb-3 mt-3">
                        <input type="phone" class="form-control" name="society_phone" id="society_phone" value="{{$user->society_phone ?? old('society_phone')}}" required>
                        <label for="society_phone">Numéro de téléphone</label>
                    </div>
                    <div class="col-12 text-center mb-3 mt-3">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
