@extends('auth.template')
@section('auth.admin.users.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-12 mt-5 table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Statut</th>
                        <th>Prénom</th>
                        <th>Nom</th>
                        <th>Société</th>
                        <th>Dernière connexion</th>
                        <th>Options</th>
                    </tr>
                </thead>
                @if(!isset($ent_users) || !empty($ent_users))
                    <tbody>
                        @foreach($ent_users as $ent_user)
                        {{-- @if(empty($ent_user->archived_at)) --}}
                            <tr>
                                <th>@if(!empty($ent_user->banned_at)) <span class="text-danger">Bloqué</span> @elseif(!empty($ent_user->archived_at)) <span class="text-secondary">Archivé</span> @else <span class="text-success">Actif</span> @endif</th>
                                <td>{{$ent_user->firstname}}</td>
                                <td>{{$ent_user->lastname}}</td>
                                <td>{{$ent_user->society_name}}</td>
                                <td>{{\Carbon\Carbon::parse($ent_user->updated_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-6">
                                            <form action="{{route('users.edit')}}" method="post">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="user_id" value="{{$ent_user->id}}">
                                                @if(!empty($ent_user->banned_at))
                                                        <button type="submit" class="btn btn-success text-white rounded"><i class="far fa-share-square"></i></button>
                                                @else
                                                    <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-times-circle"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                        <div class="col-6">
                                            <form action="{{route('users.destroy')}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{$ent_user->id}}">

                                                @if(!empty($ent_user->archived_at))
                                                    <button type="submit" class="btn btn-success text-white rounded"><i class="far fa-check-circle"></i></button>
                                                @else
                                                    <button type="submit" class="btn btn-danger text-white rounded"><i class="fas fa-archive"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        {{-- @endif --}}
                        @endforeach
                    </tbody>
                    </table>
                @else
                    </table>
                    <h3 class="bold text-center">Aucun utilisateur pour l'instant</h3>
                @endif
        </div>
    </div>
</div>
@endsection
