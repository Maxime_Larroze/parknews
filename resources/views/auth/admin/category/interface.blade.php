@extends('auth.template')
@section('auth.admin.category.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-8 col-sm-8 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xxl-2 text-center">
            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#NewCategoryModal">Nouvelle catégorie</button>
        </div>
        <div class="col-12 mt-5 table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Statut</th>
                        <th>Libellé</th>
                        <th>Créée le</th>
                        <th>Dernière modification</th>
                        <th>Options</th>
                    </tr>
                </thead>
                @if(!isset($categories) || !empty($categories))
                    <tbody>
                        @foreach($categories as $categorie)
                            <tr>
                                <th>@if(!empty($categorie->archived_at)) <span class="text-secondary">Archivé</span> @else <span class="text-success">Actif</span> @endif</th>
                                <td>{{$categorie->label}}</td>
                                <td>{{\Carbon\Carbon::parse($categorie->created_at)->format('d/m/Y H:i')}}</td>
                                <td>{{\Carbon\Carbon::parse($categorie->updated_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-4">
                                            <form action="{{route('categories.edit')}}" method="post">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="categorie_id" value="{{$categorie->id}}">
                                                @if(!empty($categorie->archived_at))
                                                        <button type="submit" class="btn btn-success text-white rounded"><i class="fas fa-upload"></i></button>
                                                @else
                                                    <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-times-circle"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                        <div class="col-4">
                                            <a data-bs-toggle="modal" data-bs-target="#UpdateCategorieModal_{{$categorie->id}}"><button class="btn btn-primary text-white rounded"><i class="far fa-edit"></i></button></a>
                                            @include('modals.update_categorie')
                                        </div>
                                        <div class="col-4">
                                            <form action="{{route('categories.destroy')}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="categorie_id" value="{{$categorie->id}}">
                                                <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-trash-alt"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                @else
                    </table>
                    <h3 class="bold text-center">Aucune catégorie pour l'instant</h3>
                @endif
        </div>
    </div>
</div>
@include('modals.new_categorie')
@endsection
