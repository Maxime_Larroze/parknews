@include('template.header')
<body class="user-select-none bg-light">
@include('template.navbar-top')
@include('template.navbar-left')

<main class="content">
    <div class="container p-0">
        @error('error')
            <div class="alert alert-danger alert-outline alert-dismissible mt-3" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-icon"><i class="fas fa-exclamation-triangle"></i></div>
                <div class="alert-message text-center">{{$message}}</div>
            </div>
        @enderror
        @error('success')
            <div class="alert alert-success alert-outline alert-dismissible mt-3" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-icon"><i class="fas fa-check"></i></div>
                <div class="alert-message text-center">{{$message}}</div>
            </div>
        @enderror
    </div>
    <div class="container-fluid p-0">
        @yield('auth.home.menu.show')
        @yield('auth.client.interface.show')
        @yield('auth.profil.interface.show')
        @yield('auth.newsletter.interface.show')
        @yield('auth.admin.category.interface.show')
        @yield('auth.admin.users.interface.show')
        @yield('auth.mailing.interface.show')
        @yield('auth.newsletter.publish.show')

    </div>
</main>
@include('template.footer')
