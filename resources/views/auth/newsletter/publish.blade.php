@extends('auth.template')
@section('auth.newsletter.publish.show')
<div class="container">
    @if($newsletters->isEmpty())
        <h3 class="text-danger text-center">Vous n'avez pas encore édité de newsletter</h3>
    @else
    <div class="container mt-5">
        <p><a href="" data-bs-toggle="collapse" data-bs-target="#info">Informations <i class="far fa-question-circle text-primary"></i></a></p>
        <div id="info" class="collapse">
        <p>
            Cette page vous permet d'envoyer une newsletter spécifique avec un modèle de mailing pré-définis ou que vous aurez conçut avant. <br>
            Si vous ne trouvez pas le nom de votre newsletter dans la liste ci-dessous, vérifiez que votre newsletter est marquée comme publiée <a href="{{route('newsletters.show')}}">ici</a> <br>
            <br>
            Vous pouvez également utiliser les modèles de mailing publique (marqué comme modèle publique), mis à disposition par le système et la communauté, permettant de personnaliser vos campagnes de mailing. <br>
            Vous pouvez tester ces modèles de mailing dans votre <a href="{{route('mailings.show')}}">espace de création de modèles</a>
        </p>
        </div>
    </div>
        <form action="{{route('newsletters.send')}}" method="post">
            @csrf
            <div class="row mt-2">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 mt-5 form-floating mb-3">
                    <select class="form-select form-control" name="newsletter_id" id="newsletter_id" required>
                        @foreach ($newsletters as $newsletter)
                            <option value="{{$newsletter->id}}">{{$newsletter->title}}</option>
                        @endforeach
                    </select>
                    <label for="newsletter_id">Newsletter à publier</label>
                    @error('newsletter_id')
                        <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 mt-5 form-floating mb-3">
                    <select class="form-select form-control" name="model_id" id="model_id" required>
                        @foreach ($models as $model)
                            <option value="{{$model->id}}">{{$model->libelle}} @if(!empty($model->published_at)) (Modèle publique) @endif</option>
                        @endforeach
                    </select>
                    <label for="model_id">Newsletter à publier</label>
                    @error('model_id')
                        <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-12 mt-5 mb-3 text-center">
                <button class="btn btn-primary rounder">Envoyer la newsletter</button>
            </div>
        </form>
    @endif
</div>
@endsection
