@extends('auth.template')
@section('auth.newsletter.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-12">

                <form action="{{route('newsletters.update')}}" method="post">
                    <div class="modal-header">
                        <h4 class="modal-title">Modifier une newsletter</h4>
                        <button onclick="replaceInput()" class="btn btn-primary">Enregistrer</button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="newsletter_id" value="{{$newsletter->id}}">
                        <div class="row">
                            <div class="col-6 form-floating mb-3 mt-3">
                                <input type="text" class="form-control" name="title" id="title" placeholder="Titre de la newsletter" value="{{$newsletter->title ?? old('title')}}" required>
                                <label for="title">Titre de la newsletter</label>
                                @error('title')
                                    <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-6 form-floating mb-3 mt-3">
                                <select class="form-select form-control" name="category_id" id="category_id" required>
                                    @foreach ($categories as $categorie)
                                        <option value="{{$categorie->id}}" @if($categorie->id === $newsletter->category_id) selected @endif>{{$categorie->label}}</option>
                                    @endforeach
                                </select>
                                <label for="category_id">Catégore de la newsletter</label>
                                @error('category_id')
                                    <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 form-floating mb-3 mt-3">
                                <div class="ql-toolbar ql-snow" id="toolbar"><span class="ql-formats"><span class="ql-header ql-picker"><span class="ql-picker-label"><svg viewBox="0 0 18 18"> <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon> <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon> </svg></span><span class="ql-picker-options"><span class="ql-picker-item" data-value="1"></span><span class="ql-picker-item" data-value="2"></span><span class="ql-picker-item"></span></span></span><select class="ql-header" style="display: none;"><option value="1"></option><option value="2"></option><option selected="selected"></option></select></span><span class="ql-formats"><button type="button" class="ql-bold"><svg viewBox="0 0 18 18"> <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path> <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path> </svg></button><button type="button" class="ql-italic"><svg viewBox="0 0 18 18"> <line class="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line> <line class="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line> <line class="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line> </svg></button><button type="button" class="ql-underline"><svg viewBox="0 0 18 18"> <path class="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path> <rect class="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect> </svg></button></span><span class="ql-formats"><button type="button" class="ql-image"><svg viewBox="0 0 18 18"> <rect class="ql-stroke" height="10" width="12" x="3" y="4"></rect> <circle class="ql-fill" cx="6" cy="7" r="1"></circle> <polyline class="ql-even ql-fill" points="5 12 5 11 7 9 8 10 11 7 13 9 13 12 5 12"></polyline> </svg></button><button type="button" class="ql-code-block"><svg viewBox="0 0 18 18"> <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline> <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline> <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line> </svg></button></span></div>
                                <div id="editor">
                                    <p>{!!$newsletter->description??old('description')!!}</p>
                                </div>
                                <input type="hidden" name="description" id="descriptioneditor">
                                @error('description')
                                    <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                                @enderror
                            </div>
                            {{-- <div class="col-12 form-floating mb-3 mt-3">
                                <input type="text" rows="5" class="form-control" name="link_picture" id="link_picture" placeholder="Insérer un lien d'une photo" required value="{{$newsletter->link_picture ?? old('link_picture')}}" />
                                <label for="link_picture">Insérer le lien d'une photo</label>
                                @error('link_picture')
                                    <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                                @enderror
                            </div> --}}
                            <div class="col-12 form-floating mb-3 mt-5 input-group">
                                <input type="text" class="form-control" name="link" id="link" placeholder="Insérer un lien" required value="{{$newsletter->link ?? old('link')}}" />
                                <label for="link">Insérer un lien</label>
                                <select class="form-select form-control" name="link_type">
                                    <option value="link" @if($newsletter->link_type === 'link') selected @endif>Lien</option>
                                    <option value="picture" @if($newsletter->link_type === 'picture') selected @endif>Image</option>
                                    <option value="video" @if($newsletter->link_type === 'video') selected @endif>Vidéo</option>
                                    </select>
                                @error('link')
                                    <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="mb-5"></div>
                </form>
        </div>
    </div>
</div>
<script>
    const editorUpdate = new window.Quill('#editor', {
        modules: { toolbar: '#toolbar' },
        placeholder: 'Ecrivez votre article ici',
        readOnly: false,
        theme: 'snow',
        debug: 'info',
    });
</script>
<script>
    function replaceInput(){
        document.getElementById('descriptioneditor').value = document.getElementsByClassName('ql-editor')[0].innerHTML;
        HTMLFormElement.submit()
    }
</script>
@endsection
