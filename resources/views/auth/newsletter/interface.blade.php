@extends('auth.template')
@section('auth.newsletter.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-12 mb-5">
            <p><a href="" data-bs-toggle="collapse" data-bs-target="#info">Informations <i class="far fa-question-circle text-primary"></i></a></p>
            <div id="info" class="collapse">
                <p>
                    Cette page vous permet d'éditer, publier et modifier vos newsletters. <br>
                    <br>
                    Nous avons mis en place un éditeur appelé "richtext" vous permettant d'éditer vos newsletter comme vous le voulez et comme vous l'entendez !
                    <br>
                    Nous gardons toujours en tête l'expérience utilisateur et nous souhaitons également concervoir cette plateforme de manière évolutive.
                    C'est pourquoi, nous mettons tout en oeuvre pour que cette plateforme vous ressemble au mieu.
                </p>
            </div>
        </div>
        <div class="col-8 col-sm-8 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
            {{-- <div class="input-group">
                <input class="form-control" type="text" placeholder="Rechercher" aria-label="Rechercher" aria-describedby="btnNavbarSearch">
                <button class="btn btn-primary border-end border-white" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
            </div> --}}
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xxl-2 text-center">
            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#NewNewsletterModal">Nouvelle newsletter</button>
        </div>
        <div class="col-12 mt-5 table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Statut</th>
                        <th>Titre</th>
                        <th>Ecrite le</th>
                        <th>Dernière modification</th>
                        <th>Options</th>
                    </tr>
                </thead>
                @if(!$newsletters->isEmpty())
                    <tbody>
                        @foreach($newsletters as $newsletter)
                            <tr>
                                <td>@if(empty($newsletter->published_at)) <span class="text-warning">Non publié</span> @else <span class="text-success">Publié</span> @endif</td>
                                <td><a href="{{URL::signedRoute('public.newsletters.signed', ['id'=>$newsletter->id])}}">{{$newsletter->title}}</a></td>
                                <td>{{\Carbon\Carbon::parse($newsletter->created_at)->format('d/m/Y H:i')}}</td>
                                <td>{{\Carbon\Carbon::parse($newsletter->updated_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                                            <form action="{{route('newsletters.edit')}}" method="post">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="newsletter_id" value="{{$newsletter->id}}">
                                                @if(empty($newsletter->published_at))
                                                        <button type="submit" class="btn btn-success text-white rounded"><i class="fas fa-upload"></i></button>
                                                @else
                                                    <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-times-circle"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                                            <a href="{{route('newsletters.showUpdate', $newsletter->id)}}"><button class="btn btn-primary text-white rounded"><i class="far fa-edit"></i></button></a>
                                        </div>
                                        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                                            <form action="{{route('newsletters.destroy')}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="newsletter_id" value="{{$newsletter->id}}">
                                                <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-trash-alt"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                @else
                    </table>
                    <h3 class="bold text-center">Aucune newsletter écrite pour l'instant</h3>
                @endif
        </div>
    </div>
</div>
@include('modals.new_newsletter')
@endsection
