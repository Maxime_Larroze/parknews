@extends('auth.template')
@section('auth.mailing.interface.show')
<div class="container">
    <div class="row mt-5">
        <div class="col-sm-0 col-md-6 col-lg-10 col-xl-10 col-xxl-10">
            <p><a href="" data-bs-toggle="collapse" data-bs-target="#info">Informations <i class="far fa-question-circle text-primary"></i></a></p>
            <div id="info" class="collapse">
            <p>
                Cette page vous permet de concevoir, éditer, publier et tester vos idées de modèle de mailing. <br>
                <br>
                Le modèle de mailing est utilisé lorsque vous envoyé une newsletter à votre base de données clients. Elle permet de personnaliser le format et l'affichage de votre newsletter, et ainsi de vous démarquer de vos concurrents. <br>
                <br>
                Sur cette page, vous pouvez poster et publier votre modèle de mailing au format HTML. <br>
                Nous avons développé un système simple et évolutif, vous permettant de personnaliser de A à Z vos modèles de mailing.
                <br><br>
                <h3 class="text-center">Tableau d'utilisation des balises</h3>
                <div class="table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">
                    <table class="table table-hover text-center">
                        <thead>
                            <tr>
                                <th>Balise</th>
                                <th>Explication</th>
                                {{-- <th>Option</th> --}}
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td id="balisetitre">[[TITRE]]</td>
                                <td>Balise permettant d'afficher le titre de votre article (newsletter)</td>
                                {{-- <td><button class="btn btn-primary" onclick="copy('balisetitre')">Copier la balise</button></td> --}}
                            </tr>
                            <tr>
                                <td>[[CATEGORIE]]</td>
                                <td>Balise permettant d'afficher la catégorie associée à votre article</td>
                            </tr>
                            <tr>
                                <td>[[ARTICLE]]</td>
                                <td>Balise permettant d'afficher votre article comme vous l'avez imaginé</td>
                            </tr>
                            <tr>
                                <td>[[NOMGROUPE]]</td>
                                <td>Balise permettant d'afficher le nom de la société pour laquelle vous publiez vos articles</td>
                            </tr>
                            <tr>
                                <td>[[MAILGROUPE]]</td>
                                <td>Balise permettant d'afficher l'adresse email de la société pour laquelle vous publiez vos articles</td>
                            </tr>
                            <tr>
                                <td>[[TELGROUPE]]</td>
                                <td>Balise permettant d'afficher le numéro de téléphone de la société pour laquelle vous publiez vos articles</td>
                            </tr>
                            <tr>
                                <td>[[NOMEDITEUR]]</td>
                                <td>Balise permettant d'afficher votre nom pour vous identifier lorsque vous publiez vos articles</td>
                            </tr>
                            <tr>
                                <td>[[DESINSCRIPTION]]</td>
                                <td>Balise permettant d'afficher ou d'utiliser un lien de désinscription automatique (obligatoire) dans les campagnes de mailing</td>
                            </tr>
                          </tbody>
                    </table>
                </div>
            </p>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-2 col-xl-2 col-xxl-2 text-center">
            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#NewModelModal">Nouveau modèle</button>
        </div>
        <div class="col-12 mt-5 table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Statut</th>
                        <th>Libellé</th>
                        <th>Créé le</th>
                        <th>Dernière modification</th>
                        <th>Options</th>
                    </tr>
                </thead>
                @if(!isset($templates) || !empty($templates))
                    <tbody>
                        @foreach($templates as $template)
                            <tr>
                                <th>@if(empty($template->published_at)) <span class="text-warning">Privé</span> @else <span class="text-success">Publique</span> @endif</th>
                                <td>{{$template->libelle}}</td>
                                <td>{{\Carbon\Carbon::parse($template->created_at)->format('d/m/Y H:i')}}</td>
                                <td>{{\Carbon\Carbon::parse($template->updated_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <div class="row">
                                        @if((!empty($template->published_at) && Auth::user()->id === $template->user_id) || (empty($template->published_at) && Auth::user()->id === $template->user_id))
                                            <div class="col-3">
                                                <form action="{{route('mailings.edit')}}" method="post">
                                                    @csrf
                                                    @method('PUT')
                                                    <input type="hidden" name="model_id" value="{{$template->id}}">
                                                    @if(empty($template->published_at))
                                                            <button type="submit" class="btn btn-success text-white rounded"><i class="fas fa-upload"></i></button>
                                                    @else
                                                        <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-times-circle"></i></button>
                                                    @endif
                                                </form>
                                            </div>
                                        @endif
                                        <div class="col-3">
                                            <form action="{{route('mailings.send')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="model_id" value="{{$template->id}}">
                                                <button class="btn btn-warning text-white rounded" type="submit"><i class="fas fa-cubes text-white"></i></button>
                                            </form>
                                        </div>
                                        @if(empty($template->published_at))
                                            <div class="col-3">
                                                <a data-bs-toggle="modal" data-bs-target="#UpdateModelModal_{{$template->id}}"><button class="btn btn-primary text-white rounded"><i class="far fa-edit"></i></button></a>
                                                @include('modals.update_mailing')
                                            </div>
                                            <div class="col-3">
                                                <form action="{{route('mailings.destroy')}}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <input type="hidden" name="model_id" value="{{$template->id}}">
                                                    <button type="submit" class="btn btn-danger text-white rounded"><i class="far fa-trash-alt"></i></button>
                                                </form>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                @else
                    </table>
                    <h3 class="bold text-center">Aucune modèle de mailing disponible pour l'instant</h3>
                @endif
            </table>
        </div>
    </div>
</div>
@include('modals.new_model')
<script>
function copy(arg) {
    const copyText = document.getElementById(arg);
    const el = document.createElement('textarea');
    el.value = copyText.innerHTML;
    el.select();
    document.execCommand('copy');
    alert("Balise copiée: " + el.value);
}
</script>
@endsection
