@php
$cat = "Catégorie non renseignée";
foreach($categories as $categorie)
{
    if($categorie->id === $article->category_id)
    {
        $cat = $categorie->label;
    }
}

$header = str_replace("[[TITRE]]", $article->title, $newsletter->header);
$header = str_replace("[[CATEGORIE]]", $cat, $header);
$header = str_replace("[[NOMGROUPE]]", $user->society_name, $header);
$header = str_replace("[[NOMEDITEUR]]", $user->firstname.' '.$user->lastname, $header);
$header = str_replace("[[MAILGROUPE]]", $user->society_email, $header);
$header = str_replace("[[TELGROUPE]]", $user->society_phone, $header);
$header = str_replace("[[DESINSCRIPTION]]", URL::signedRoute('public.unsubscribe.signed',['user'=>$user->id, 'mail'=>$client_email]), $header);
$header = str_replace("[[ARTICLE]]", $article->description, $header);
$header = str_replace("[[LIENARTICLE]]", URL::signedRoute('public.newsletters.signed', ['id'=>$article->id]), $header);

$body = str_replace("[[TITRE]]", $article->title, $newsletter->body);
$body = str_replace("[[CATEGORIE]]", $cat, $body);
$body = str_replace("[[NOMGROUPE]]", $user->society_name, $body);
$body = str_replace("[[NOMEDITEUR]]", $user->firstname.' '.$user->lastname, $body);
$body = str_replace("[[MAILGROUPE]]", $user->society_email, $body);
$body = str_replace("[[TELGROUPE]]", $user->society_phone, $body);
$body = str_replace("[[DESINSCRIPTION]]", URL::signedRoute('public.unsubscribe.signed',['user'=>$user->id, 'mail'=>$client_email]), $body);
$body = str_replace("[[ARTICLE]]", $article->description, $body);
$body = str_replace("[[LIENARTICLE]]", URL::signedRoute('public.newsletters.signed', ['id'=>$article->id]), $body);

$footer = str_replace("[[TITRE]]", $article->title, $newsletter->footer);
$footer = str_replace("[[CATEGORIE]]", $cat, $footer);
$footer = str_replace("[[NOMGROUPE]]", $user->society_name, $footer);
$footer = str_replace("[[NOMEDITEUR]]", $user->firstname.' '.$user->lastname, $footer);
$footer = str_replace("[[MAILGROUPE]]", $user->society_email, $footer);
$footer = str_replace("[[TELGROUPE]]", $user->society_phone, $footer);
$footer = str_replace("[[DESINSCRIPTION]]", URL::signedRoute('public.unsubscribe.signed',['user'=>$user->id, 'mail'=>$client_email]), $footer);
$footer = str_replace("[[ARTICLE]]", $article->description, $footer);
$footer = str_replace("[[LIENARTICLE]]", URL::signedRoute('public.newsletters.signed', ['id'=>$article->id]), $footer);

@endphp


{!! $header !!}
{!! $body !!}
{!! $footer !!}
