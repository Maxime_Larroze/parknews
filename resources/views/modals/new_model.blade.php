<div class="modal fade" id="NewModelModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{route('mailings.store')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Nouveau modèle de mailing</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body form-floating">
                    @csrf
                    <div class="form-floating mb-3 mt-3">
                        <input type="text" class="form-control" name="libelle" id="libelle" placeholder="Nom du modèle" value="{{old('libelle')}}" required>
                        <label for="label">Nom du modèle</label>
                    </div>
                    @error('label')
                        <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                    @enderror

                    <div class="form-floating mb-3 mt-3">
                        <textarea class="form-control" name="header" id="header" placeholder="Header" required>{{old('body')}}</textarea>
                        <label for="header">Header</label>
                        @error('header')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3 mt-3">
                        <textarea class="form-control" name="body" id="body" placeholder="Body" required>{{old('body')}}</textarea>
                        <label for="body">Body</label>
                        @error('body')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3 mt-3">
                        <textarea class="form-control" name="footer" id="footer" placeholder="Footer" required>{{old('footer')}}</textarea>
                        <label for="footer">Footer</label>
                        @error('footer')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>
