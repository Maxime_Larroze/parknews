<div class="modal fade" id="NewClientModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('clients.store')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Nouveau client</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                        @csrf
                        <div class="form-floating mb-3 mt-3">
                            <input type="email" class="form-control" name="email_client" id="email_client" placeholder="Adresse email du client" required>
                            <label for="email_client">Adresse email</label>
                        </div>
                        @error('email_client')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>
