<div class="modal fade" id="UpdateModelModal_{{$template->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{route('mailings.update')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Modifier un modèle de mailing</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body form-floating">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="model_id" value="{{$template->id}}">
                    <div class="form-floating mb-3 mt-3">
                        <input type="text" class="form-control" name="libelle" id="libelle" placeholder="Nom du modèle" value="{{$template->libelle ?? old('libelle')}}" required>
                        <label for="libelle">Nom du modèle</label>
                    </div>
                    @error('libelle')
                        <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                    @enderror

                    <div class="form-floating mb-3 mt-3">
                        <textarea class="form-control" name="header" id="header" placeholder="Header" required>{{$template->header ?? old('body')}}</textarea>
                        <label for="header">Header</label>
                        @error('header')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3 mt-3">
                        <textarea class="form-control" name="body" id="body" placeholder="Body" required>{{$template->body ?? old('body')}}</textarea>
                        <label for="body">Body</label>
                        @error('body')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3 mt-3">
                        <textarea class="form-control" name="footer" id="footer" placeholder="Footer" required>{{$template->footer ?? old('footer')}}</textarea>
                        <label for="footer">Footer</label>
                        @error('footer')
                            <div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>
