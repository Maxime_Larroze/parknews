<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'register_type',
        'email',
        'archived_at',
        'created_at',
        'updated_at',
    ];
}
