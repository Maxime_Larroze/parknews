<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'libelle',
        'header',
        'body',
        'footer',
        'archived_at',
        'published_at',
        'created_at',
        'updated_at',
    ];
}
