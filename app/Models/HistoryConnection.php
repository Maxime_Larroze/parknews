<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryConnection extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'ip_address',
        'user_agent',
        'created_at',
        'updated_at',
    ];
}
