<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'category_id',
        'title',
        'link',
        'link_type',
        'link_picture',
        'description',
        'published_at',
        'created_at',
        'updated_at',
    ];

    public function editor() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
