<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TestNSMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $newsletter;
    public $article;
    public $categories;
    public $client_email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $newsletter, $article, $categories, $client_email)
    {
        $this->user = $user;
        $this->newsletter = $newsletter;
        $this->article = $article;
        $this->categories = $categories;
        $this->client_email = $client_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.test-newsletter')->subject($this->article->title)->with(['user' => $this->user,
        'newsletter' => $this->newsletter, 'article' => $this->article, 'categories'=>$this->categories, 'client_email'=>$this->client_email]);
    }
}
