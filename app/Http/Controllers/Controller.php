<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="API Park News Plateforme",
     *      description="Information API via Swagger de la plateforme Park News",
     *      @OA\Contact(
     *          email="maxime.larrozefrancezat@ynov.com"
     *      ),
     *      @OA\License(
     *          name="Propriétaire Groupe EHE / Horizon Group",
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="API Serveur"
     * )
    *
    *
    */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
