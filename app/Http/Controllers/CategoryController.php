<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('auth.admin.category.interface', ['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'label' => 'required',
        ]);
        Category::create(['label'=>$request->label]);
        return redirect()->route('categories.index')->withErrors(['success'=>'Nouvelle catégorie créée']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request, [
            'categorie_id' => 'required',
        ]);
        $cat = Category::find($request->categorie_id);
        if(empty($cat->archived_at)){$cat->update(['archived_at'=>Carbon::now()]);}
        else{$cat->update(['archived_at'=>null]);}
        return redirect()->route('categories.index')->withErrors(['success'=>'Catégorie mise à jour avec succès']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCategoryRequest  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'label' => 'required',
            'categorie_id' => 'required',
        ]);
        Category::find($request->categorie_id)->update(['label'=>$request->label]);
        return redirect()->route('categories.index')->withErrors(['success'=>'Catégorie modifié avec succès']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'categorie_id' => 'required',
        ]);
        Category::find($request->categorie_id)->delete();
        return redirect()->route('categories.index')->withErrors(['success'=>'Catégorie supprimée avec succès']);
    }
}
