<?php

namespace App\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class GithubController extends Controller
{
    /**
     * function to redirect to Google Auth.
     *
     * @return void
     */
    public function redirectToGithub()
    {
        Log::info("Tentative de connexion via Github");
        return Socialite::driver('github')->redirect();
    }

    /**
     * function to login or register with google.
     *
     * @return void
     */
    public function handleGithubCallback()
    {
        try {
            $user = Socialite::driver('github')->user();
            $userFounded = User::updateOrCreate(
                [
                    'firstname' => $user->user['login'],
                    'email' => $user->user['email'],
                    'picture' => $user->user['avatar_url'],
                ]
            );
            Auth::login($userFounded, true);
            Log::info("Connexion via Github ".Auth::user()->id);
            if(!empty(Auth::user()->banned_at))
            {
                Log::critical("Connexion utilisateur Facebook banni: ".Auth::user()->id);
                Auth::logout();
                return redirect()->route('login')->withErrors(['error'=>"Votre compte à été bloqué. Contactez le service administration."]);
            }
            if(!empty(Auth::user()->archived_at))
            {
                Auth::logout();
                return redirect()->route('login')->withErrors(['error'=>"Votre compte à été archivé."]);
            }
            return redirect()->route('home');
        } catch (Exception $e) {
            Log::critical("Erreur de connexion Github: ".$e);
            return redirect()->route('login')->withErrors(['error'=>"Impossible de vous connecter. Veuillez vérifier votre identifiant / mot de passe"]);
        }
    }
}
