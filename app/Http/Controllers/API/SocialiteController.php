<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;

class SocialiteController extends Controller
{
    /**
     * @OA\Get(
     *      path="/v1/login",
     *      operationId="redirectDriver",
     *      tags={"Connexion"},
     *      summary="Login",
     *      description="Connexion de l'utilisateur via le endpoint défini : Facebook, Google, Github",
     *      @OA\Property(
     *          property="driver",
     *          type="string",
     *          description="Nom du driver"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function redirectDriver()
    {
        try {
            Log::info("Tentative de connexion via google");
        return Socialite::driver('google')->redirect();
        } catch (\Throwable $th) {
        }

        try {
            Log::info("Tentative de connexion via facebook");
        return Socialite::driver('facebook')->redirect();
        } catch (\Throwable $th) {
        }

        try {
            Log::info("Tentative de connexion via github");
        return Socialite::driver('github')->redirect();
        } catch (\Throwable $th) {
        }
    }

    /**
     * @OA\Get(
     *      path="/v1/callback",
     *      operationId="handleCallback",
     *      tags={"Connexion"},
     *      summary="Redirection login",
     *      description="Redirection après conenxion via Socialite",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function handleCallback()
    {
        try {
            $this->handleGithubCallback();
        } catch (Exception $e) {
        }

        try {
            $this->handleGoogleCallback();
        } catch (Exception $e) {
        }

        try {
            $this->handleFacebookCallback();
        } catch (Exception $e) {
        }
        return response('Impossible de vous connecter. Veuillez vérifier votre identifiant / mot de passe', 401);
    }


    public function handleGithubCallback()
    {
        try {
            $user = Socialite::driver('github')->user();
            $userFounded = User::updateOrCreate(
                ['email' =>  $user->user['email']],
                [
                    'firstname' => $user->user['login'],
                    'lastname' => '',
                    'email' => $user->user['email'],
                    'picture' => $user->user['avatar_url'],
                ]
            );
            Auth::login($userFounded, true);
            Log::info("Connexion via Github ".Auth::user()->id);
            return response($userFounded,200);;
        } catch (Exception $e) {
        }
    }
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $userFounded = User::updateOrCreate(
                ['email' =>  $user->user['email']],
                [
                    'firstname' => $user->user['given_name'],
                    'lastname' => $user->user['family_name'],
                    'email' => $user->user['email'],
                    'picture' => $user->user['picture'],
                ]
            );
            Auth::login($userFounded, true);
            Log::info("Connexion via Github ".Auth::user()->id);
            return response($userFounded,200);;
        } catch (Exception $e) {
        }
    }
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $userFounded = User::updateOrCreate(
                ['email' =>  $user->user['email']],
                [
                    'firstname' => $user->user['name'],
                    'lastname' => '',
                    'email' => $user->user['email'],
                    'picture' => $user->avatar_original,
                ]
            );
            Auth::login($userFounded, true);
            Log::info("Connexion via Github ".Auth::user()->id);
            return response($userFounded,200);;
        } catch (Exception $e) {
        }
    }
}
