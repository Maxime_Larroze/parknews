<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Newsletter;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $cats = Category::all();
        $nss = Newsletter::where('published_at','!=',null)->get();
        $result = [];
        foreach($cats as $cat)
        {
            $tmp = 0;
            foreach($nss as $ns)
            {
                if($cat->id === $ns->category_id){$tmp++;}
            }
            $result[(string)$cat->label] = $tmp;
        }
        return response($result, 200);
    }
}
