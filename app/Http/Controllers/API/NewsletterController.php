<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $t30 = Carbon::now()->subDays(30);
        $nss = Newsletter::where('published_at', '!=', null)->where('published_at','>=', $t30)->get();
        $result = [];
        for ($i=0; $i <30 ; $i++) {
            if($i===0)
            {
                $result[(string)Carbon::now()->format('d')]=0;
            }
            elseif($i===1)
            {
                $result[(string)Carbon::now()->subDay()->format('d')]=0;
            }
            elseif($i>1)
            {
                $result[(string)Carbon::now()->subDays($i)->format('d')] =0;
            }
        }
        for ($i=0; $i <30 ; $i++) {
            foreach($nss as $ns)
            {
                if(Carbon::parse($ns->created_at)->format('d') === $result[(string)Carbon::now()->format('d')])
                {
                    $result[(string)Carbon::now()->format('d')]++;
                }
                elseif($i === 1 && Carbon::parse($ns->created_at)->format('d') === $result[(string)Carbon::now()->subDay()->format('d')])
                {
                    $result[(string)Carbon::now()->subDay()->format('d')]++;
                }
                elseif($i > 1 && Carbon::parse($ns->created_at)->format('d') === $result[(string)Carbon::now()->subDays($i)->format('d')])
                {
                    $result[(string)Carbon::now()->subDays($i)->format('d')] ++;
                }
            }
        }
        return response($result,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNewsletterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return response(Newsletter::where('published_at', '!=', null)->orderBy('created_at', 'desc')->get(),200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit(Newsletter $newsletter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNewsletterRequest  $request
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsletter $newsletter)
    {
        //
    }
}
