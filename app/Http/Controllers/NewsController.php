<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class NewsController extends Controller
{
    public $URI = 'https://newsapi.org/v2/';
    public $APP_KEY = '&apiKey=7c3a0449e39444c4b6b58576438461d6';

    public function show()
    {
        $response = Http::get($this->URI."everything?q=informatique&language=fr&sortBy=popularity".$this->APP_KEY);
        return view('public.blog.news', [
            'news'=>$response->json()['articles']
        ]);
    }

    public function search(Request $request)
    {
        $response = Http::get($this->URI."everything?q=".$request->search."&language=fr&sortBy=popularity".$this->APP_KEY);
        return view('public.blog.news', [
            'news'=>$response->json()['articles']
        ]);
    }
}
