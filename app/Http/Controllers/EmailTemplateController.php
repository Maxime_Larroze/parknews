<?php

namespace App\Http\Controllers;

use App\Mail\TestNSMail;
use App\Models\Category;
use App\Models\EmailTemplate;
use App\Models\Newsletter;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEmailTemplateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'libelle' => 'required',
            'header' => 'required',
            'body' => 'required',
            'footer' => 'required',
        ]);
        EmailTemplate::create([
            'user_id'=>Auth::user()->id,
            'libelle'=>$request->libelle,
            'header'=>$request->header,
            'body'=>$request->body,
            'footer'=>$request->footer,
        ]);
        return redirect()->route('mailings.show')->withErrors(['success'=>'Modèle enregistré avec succès']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $templates = EmailTemplate::where('user_id', Auth::user()->id)->orWhere('published_at','!=',null)->get();
        return view('auth.mailing.interface', ['templates'=>$templates]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request, [
            'model_id' => 'required',
        ]);

        $model = EmailTemplate::find($request->model_id);
        if(!empty($model->published_at)){$model->update(['published_at'=>null]);}
        else{$model->update(['published_at'=>Carbon::now()]);}
        return redirect()->route('mailings.show')->withErrors(['success'=>'Modèle mise à jour']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEmailTemplateRequest  $request
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'libelle' => 'required',
            'header' => 'required',
            'body' => 'required',
            'footer' => 'required',
        ]);
        EmailTemplate::find($request->model_id)->update([
            'libelle'=>$request->libelle,
            'header'=>$request->header,
            'body'=>$request->body,
            'footer'=>$request->footer,
        ]);
        return redirect()->route('mailings.show')->withErrors(['success'=>'Modèle mis à jour avec succès']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'model_id' => 'required',
        ]);
        $ns = EmailTemplate::find($request->model_id)->delete();
        return redirect()->route('mailings.show')->withErrors(['success'=>'Modèle supprimé avec succès']);
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'model_id' => 'required',
        ]);
        $categories = Category::all();
        $template = EmailTemplate::find($request->model_id);
        $user = User::find(Auth::user()->id);
        $article = Newsletter::where('user_id',$user->id)->orderBy('id', 'DESC')->first();
        Mail::to($user->email)->send(new TestNSMail($user, $template, $article, $categories, $user->email));
        return redirect()->route('mailings.show')->withErrors(['success'=>'Mail de test envoyé avec succès']);
    }
}
