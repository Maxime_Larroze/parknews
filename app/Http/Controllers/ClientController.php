<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::where('user_id', Auth::user()->id)->get();
        return view('auth.client.interface', ['clients'=>$clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'newsletter_id'=>'required',
            'user_id' => 'required',
            'email' => 'required',
        ]);
        $client = Client::where('user_id', $request->user_id)->where('email', $request->email)->first();
        if(!empty($client))
        {
            return redirect(URL::signedRoute('public.newsletters.signed', ['id'=>$request->newsletter_id]))->withErrors(['error'=>'Votre adresse email déjà été enregistrée pour ce groupe']);
        }
        else{
            Client::create([
                'user_id'=>$request->user_id,
                'register_type'=>'externe',
                'email'=>$request->email
            ]);
            return redirect(URL::signedRoute('public.newsletters.signed', ['id'=>$request->newsletter_id]))->withErrors(['success'=>'Votre email à bien été enregistré pour ce groupe']);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreClientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email_client' => 'required',
        ]);
        Client::create([
            'user_id'=>Auth::user()->id,
            'register_type'=>'interne',
            'email'=>$request->email_client
        ]);
        return redirect()->route('clients.index')->withErrors(['success'=>'Client créer avec succès']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClientRequest  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required',
        ]);
        $client = Client::find($request->client_id);
        if(!empty($client->archived_at)){$client->update(['archived_at'=>null]);}
        else{$client->update(['archived_at'=>Carbon::now()]);}
        return redirect()->route('clients.index')->withErrors(['success'=>'Client modifié avec succès']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required',
        ]);
        Client::find($request->client_id)->delete();
        return redirect()->route('clients.index')->withErrors(['success'=>'Client supprimé avec succès']);
    }
}
