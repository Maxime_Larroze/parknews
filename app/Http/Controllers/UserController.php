<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ent_users = User::all();
        return view('auth.admin.user.interface', ['ent_users'=>$ent_users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreHistoryConnectionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HistoryConnection  $historyConnection
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = User::find(Auth::user()->id);
        return view('auth.profil.interface', ['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HistoryConnection  $historyConnection
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
        ]);
        $user = User::find($request->user_id);
        if(empty($user->banned_at)){$user->update(['banned_at'=>Carbon::now()]);}
        else{$user->update(['banned_at'=>null]);}
        return redirect()->route('users.index')->withErrors(['success'=>'Utilsiateur modifié avec succès']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateHistoryConnectionRequest  $request
     * @param  \App\Models\HistoryConnection  $historyConnection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'society_name' => 'required',
        ]);
        $user = User::find(Auth::user()->id)->update([
            'society_name'=>$request->society_name,
            'society_email'=>$request->society_email,
            'society_phone'=>$request->society_phone
        ]);
        return redirect()->route('me.show')->withErrors(['success'=>'Profil enregistré']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HistoryConnection  $historyConnection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
        ]);
        $user = User::find($request->user_id);
        if(empty($user->archived_at)){$user->update(['archived_at'=>Carbon::now()]);}
        else{$user->update(['archived_at'=>null]);}
        return redirect()->route('users.index')->withErrors(['success'=>'Utilsiateur modifié avec succès']);
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
