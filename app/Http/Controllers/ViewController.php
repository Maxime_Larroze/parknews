<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Newsletter;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ViewController extends Controller
{
    public function showLogin()
    {
        return view('public.template');
    }
    public function showApi()
    {
        return redirect('/api/documentation');
    }
    public function redirectLogin()
    {
        return redirect()->route('login');
    }
    // public function redirectHome()
    // {
    //     return redirect()->route('home');
    // }
    public function showProject()
    {
        return response()->file('pdfs/parknews.pdf');
    }

    public function showHome()
    {
        $categories = Category::all();
        $newsletters = Newsletter::where('user_id',Auth::user()->id)->orderby('created_at', 'DESC')->limit(8)->get();
        return view('auth.home.menu', ['categories'=>$categories, 'newsletters'=>$newsletters]);
    }

    public function showBlog(Request $request)
    {
        $users = User::all();
        $nb_page = substr(ceil(count(Newsletter::where('published_at', '!=', null)->get())/4),0,2);
        $newsletters = Newsletter::where('published_at', '!=', null)->orderBy('id', 'DESC')->limit(4)->paginate(4);
        return view('public.blog.list_articles', ['newsletters'=>$newsletters, 'users'=>$users, 'nb_page'=>$nb_page]);
    }
}
