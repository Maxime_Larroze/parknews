<?php

namespace App\Http\Controllers;

use App\Mail\TestNSMail;
use App\Models\Category;
use App\Models\Client;
use App\Models\EmailTemplate;
use App\Models\Newsletter;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $newsletters = Newsletter::where('published_at', '!=', null)->get();
        $newsletter = Newsletter::find($request->id);
        $ent_users = User::all();
        $categories  = Category::all();
        return view('public.blog.article', ['ent_users'=>$ent_users, 'newsletter'=>$newsletter, 'newsletters'=>$newsletters, 'categories '=>$categories ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNewsletterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'link' => 'required',
            'link_type' => 'required',
            // 'link_picture' => 'required',
            'category_id' => 'required',
        ]);
        $ns = Newsletter::create([
            'user_id'=>Auth::user()->id,
            'title'=>$request->title,
            'description'=>$request->description,
            'link'=>$request->link,
            'link_type'=>$request->link_type,
            'link_picture'=>"http://parknews.services-ehe.fr/images/logo/logo.png",
            // 'link_picture'=>$request->link_picture,
            'category_id'=>$request->category_id,
            'published_at'=>Carbon::now(),
        ])->id;
        return redirect()->route('newsletters.show')->withErrors(['success'=>'Newsletter enregistrée avec succès']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        $categories  = Category::all();
        $newsletters = Newsletter::where('user_id', $user->id)->get();
        return view('auth.newsletter.interface', ['newsletters'=>$newsletters, 'categories'=>$categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request, [
            'newsletter_id' => 'required',
        ]);
        $ns = Newsletter::find($request->newsletter_id);
        if(!empty($ns->published_at)){$ns->update(['published_at'=>null]);}
        else{$ns->update(['published_at'=>Carbon::now()]);}
        return redirect()->route('newsletters.show')->withErrors(['success'=>'Newsletter mise à jour']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNewsletterRequest  $request
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'newsletter_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'link' => 'required',
            'link_type' => 'required',
            // 'link_picture' => 'required',
            'category_id' => 'required',
        ]);
        Newsletter::find($request->newsletter_id)->update([
            'user_id'=>Auth::user()->id,
            'title'=>$request->title,
            'description'=>$request->description,
            'link'=>$request->link,
            'link_type'=>$request->link_type,
            // 'link_picture'=>$request->link_picture,
            'link_picture'=>"http://parknews.services-ehe.fr/images/logo/logo.png",
            'category_id'=>$request->category_id,
        ]);
        return redirect()->route('newsletters.show')->withErrors(['success'=>'Newsletter modifiée avec succès']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'newsletter_id' => 'required',
        ]);
        $ns = Newsletter::find($request->newsletter_id)->delete();
        return redirect()->route('newsletters.show')->withErrors(['success'=>'Newsletter surpimée avec succès']);
    }

    public function unsubscribe(Request $request)
    {
        try {
            $client = Client::where('user_id', $request->user)->where('email', $request->mail)->first();
            $client->delete();
            return redirect()->route('home')->withErrors(['success'=>'Votre désinscription de newsletter à bien été effectuée']);
        } catch (\Throwable $th) {
            return redirect()->route('home')->withErrors(['error'=>'Impossible de vous désinscrire de cette newsletter']);
        }
    }

    public function showSender()
    {
        $models = EmailTemplate::where('published_at','!=',null)->orWhere('user_id',Auth::user()->id)->get();
        $newsletters = Newsletter::where('user_id',Auth::user()->id)->get();
        return view('auth.newsletter.publish', ['models'=>$models, 'newsletters'=>$newsletters]);
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'newsletter_id' => 'required',
            'model_id' => 'required',
        ]);
        $clients = Client::where('archived_at',null)->where('user_id',Auth::user()->id)->get();
        $categories = Category::all();
        $template = EmailTemplate::find($request->model_id);
        $user = User::find(Auth::user()->id);
        $article = Newsletter::find($request->newsletter_id);
        foreach($clients as $client)
        {
            Mail::to($client->email)->send(new TestNSMail($user, $template, $article, $categories, $client->email));
        }
        return redirect()->route('newsletter.showsender')->withErrors(['success'=>'Votre newsletter à bien été envoyé par email à vos clients']);
    }

    public function showUpdate(Request $request)
    {
        $user = Auth::user();
        $categories  = Category::all();
        $newsletter = Newsletter::find($request->id);
        return view('auth.newsletter.edit', ['user'=>$user, 'categories'=>$categories, 'newsletter'=>$newsletter]);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required',
        ]);
        $users = User::all();
        $nb_page = substr(ceil(count(Newsletter::where('published_at', '!=', null)->get())/4),0,2);
        $newsletters = Newsletter::where('published_at', '!=', null)->orderBy('id', 'DESC')->limit(4)->paginate(4);


        $editeur = Newsletter::find(10)->editor;

        $resultats = Newsletter::where('title','like', '%'.$request->search.'%')
        ->orWhere('description','like', '%'.$request->search.'%')
        ->orWhere('published_at','like', '%'.$request->search.'%')

        // ->orWhere('firstname','like', '%'.$editeur->firstname.'%')
        // ->orWhere('lastname','like', '%'.$editeur->lastname.'%')
        // ->orWhere('email','like', '%'.$editeur->email.'%')
        // ->orWhere('society_name','like', '%'.$editeur->society_name.'%')
        // ->orWhere('society_email','like', '%'.$editeur->society_email.'%')
        // ->orWhere('society_phone','like', '%'.$editeur->society_phone.'%')
        ->get();
        return view('public.blog.list_articles', [
            'newsletters'=>$newsletters, 'users'=>$users, 'nb_page'=>$nb_page, 'resultats'=>$resultats, 'search'=>$request->search
        ]);
    }

}
