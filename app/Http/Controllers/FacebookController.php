<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class FacebookController extends Controller
{
    /**
     * function to redirect to Google Auth.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        Log::info("Tentative de connexion via facebook");
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * function to login or register with google.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            $userFounded = User::updateOrCreate(
                ['email' =>  $user->user['email']],
                [
                    'firstname' => $user->user['name'],
                    'lastname' => '',
                    'email' => $user->user['email'],
                    'picture' => $user->avatar_original,
                ]
            );
            Auth::login($userFounded, true);
            Log::info("Connexion via facebook ".Auth::user()->id);
            if(!empty(Auth::user()->banned_at))
            {
                Log::critical("Connexion utilisateur Facebook banni: ".Auth::user()->id);
                Auth::logout();
                return redirect()->route('login')->withErrors(['error'=>"Votre compte à été bloqué. Contactez le service administration."]);
            }
            if(!empty(Auth::user()->archived_at))
            {
                Auth::logout();
                return redirect()->route('login')->withErrors(['error'=>"Votre compte à été archivé."]);
            }
            return redirect()->route('home');
        } catch (Exception $e) {
            Log::critical("Erreur de connexion Facebook: ".$e);
            return redirect()->route('login')->withErrors(['error'=>"Impossible de vous connecter. Veuillez vérifier votre identifiant / mot de passe"]);
        }
    }
}
